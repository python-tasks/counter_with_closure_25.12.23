def create_counter(counter=0):
    def increment(inc=1):
        result=counter+inc
        return result
       
    def decrement(dec=1):
        result=counter-dec
        return result
    return increment, decrement


def get_counter_value(create_counter,calc_code="+",first_num=11,second_num=10):
    
    inc,dec = create_counter(first_num)
    if calc_code=="+":
        print(f"The sum of the {first_num} and {second_num} is: ", inc(second_num))
    elif calc_code=="-":
        print(f"The subtraction of the {first_num} and {second_num} is: ", dec(second_num)) 
    

while True:
   
    calc_code=input("Please enter the calculation code(+,-) ")
    
    if calc_code=="+" or calc_code=="-":

        try:
            first_num=int(input("Please enter the firts number: "))
            second_num=int(input("Please enter the second number: "))
            get_counter_value(create_counter, calc_code,first_num,second_num)
        except ValueError:
            print("Input values should be integers!!! ") 
            while True:
                try:
                    first_num=int(input("Please enter the firts number: "))
                    second_num=int(input("Please enter the second number: "))
                    get_counter_value(create_counter, calc_code,first_num,second_num)
                    break
                except ValueError:
                    print("Input values should be integers!!! ")
        break  
    else:
        print("Wrong code, try again!!!")    
    


